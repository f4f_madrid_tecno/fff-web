---
title: "Comunicado"
summary: "Fridays For Future está compuesto de muchas personas. Somos jóvenes, mayores, mujeres, hombres, de distintas ideologías..."
month: "Jun-19"
date: 2019-06-28T17:47:34+01:00
link: "../../../../../images/jun19/28_06.pdf"
---

<embed src="../../../../../images/jun19/28_06.pdf" type="application/pdf" width="100%" height="1000px" />

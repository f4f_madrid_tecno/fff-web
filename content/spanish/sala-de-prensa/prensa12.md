---
title: "Comunicado"
summary: "Diez protestas del movimiento Fridays For Future."
month: "May-19"
date: 2019-05-10T17:47:34+01:00
link: "../../../../../images/may19/20190510_NDP_DiezProtestas.pdf"
---

<embed src="../../../../../images/may19/20190510_NDP_DiezProtestas.pdf" type="application/pdf" width="100%" height="1000px" />

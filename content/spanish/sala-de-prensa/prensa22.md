---
title: "Comunicado ministra Teresa Ribera"
summary: "Originalmente no nos íbamos a molestar en escribir nada más largo que un tweet para contestarle, como suele ser nuestro protocolo en lo que se refiere a
miembros de la clase política diciéndonos cómo tenemos que hacer nuestro trabajo. Sin embargo, sus declaraciones de hoy nos han parecido obviamente sintomáticas de un problema mucho más grande."
month: "Dic-19"
date: 2019-12-12T12:00:00+01:00
link: "../../../../../images/dic19/191212_Comunicado_ministra.pdf"
---

<embed src="../../../../../images/dic19/191212_Comunicado_ministra.pdf" type="application/pdf" width="100%" height="1000px" >


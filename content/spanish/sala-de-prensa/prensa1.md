---
title: "Fridays For Future, más presente que nunca."
summary: "Vuelven las sentadas frente al Congreso. Este viernes 8 de noviembre, Fridays For Future volverá a sentarse frente al Congreso para protestar contra la inacción política frente a la crisis ecológica."
month: "Nov-19"
date: 2019-11-08T17:47:34+01:00
link: "../../../../../images/nov19/191108_Com_NDP.pdf"
---

<embed src="../../../../../images/nov19/191108_Com_NDP.pdf" type="application/pdf" width="100%" height="1000px" />

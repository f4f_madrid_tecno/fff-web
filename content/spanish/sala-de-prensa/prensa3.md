---
title: "Fridays For Future Madrid
condena la violencia y la represión
en todo el Estado"
summary: "Como movimiento ecologista, firmes defensoras de la no violencia, condenamos la acción represiva de las fuerzas de seguridad del Estado."
month: "Oct-19"
date: 2019-10-10T17:47:34+01:00
link: "../../../../../images/oct19/Comunicado.pdf"
---

<embed src="../../../../../images/oct19/Comunicado.pdf" type="application/pdf" width="100%" height="1000px" />

---
title: "Comunicado"
summary: "Desde ​ Fridays For Future y Juventud por el Clima Madrid tenemos el placer de anunciar
que, tras una larga jornada dedicada a la organización y reestructuración de nuestro
movimiento, se ha decidido ​ agruparnos bajo el mismo nombre: Fridays For Future
Madrid​ ."
month: "Jun-19"
date: 2019-06-16T17:47:34+01:00
link: "../../../../../images/jun19/16_06_Fusión.pdf"
---

<embed src="../../../../../images/jun19/16_06_Fusión.pdf" type="application/pdf" width="100%" height="1000px" />

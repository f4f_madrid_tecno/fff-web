---
title: "Cumbre Europea de Fridays For Future en Lausanne"
summary: "Fridays For Future Europa albergará una reunión general a principios de agosto en Lausana, Suiza, en el que participarán 450 integrantes de 37 países. El objetivo de la cumbre es fortalecer los lazos entre los participantes del movimiento Huelga Climática Joven Europea (European Youth Climate Strike) y definir el futuro de movilización ante la crisis climática."
month: "Jul-19"
date: 2019-07-12T17:47:34+01:00
link: "../../../../../images/jul19/12_07_SMILE.pdf"
---

<embed src="../../../../../images/jul19/12_07_SMILE.pdf" type="application/pdf" width="100%" height="1000px" />

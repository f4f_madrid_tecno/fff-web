
---
title: "Comunicado"
summary: "Un viernes más, nos congregamos para exigir medidas contra la crisis climática. Es el primer viernes de junio, y algunos de nosotros ya están de vacaciones."
month: "Jun-19"
date: 2019-06-28T17:47:34+01:00
link: "../../../../../images/jun19/20190607_NDP_Verano.pdf"
---

<embed src="../../../../../images/jun19/20190607_NDP_Verano.pdf" type="application/pdf" width="100%" height="1000px" />

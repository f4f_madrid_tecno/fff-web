---
title: "Fridays For Future Madrid inicia este viernes una semana de acciones que culminará con la Huelga Mundial por el Clima"
summary: "Los jóvenes vuelven con fuerza de vacaciones, con una semana de acciones que culminará con la Huelga Mundial por el Clima, convocada por más de 300 organizaciones. Del 20 al 27 de septiembre habrá en todo el país actividades
relacionadas con la emergencia climática organizadas por el colectivo."
month: "Sep-19"
date: 2019-09-19T17:47:34+01:00
link: "../../../../../images/sep19/19_09_Acciones_Madrid.pdf"
---

<embed src="../../../../../images/sep19/19_09_Acciones_Madrid.pdf" type="application/pdf" width="100%" height="1000px" />

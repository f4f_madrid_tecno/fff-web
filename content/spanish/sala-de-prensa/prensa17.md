---
title: "FFF Madrid se moviliza contra el Black Friday"
summary: "Exigen a los gobiernos y empresas una conducta responsable respecto al modelo de consumo actual. Este viernes 29 de noviembre, FFF Madrid se sentará frente al Congreso."
month: "Nov-19"
date: 2019-11-28T12:00:00+01:00
link: "../../../../../images/nov19/191129_Com_NDP_29N.pdf"
---

<embed src="../../../../../images/nov19/191129_Com_NDP_29N.pdf" type="application/pdf" width="100%" height="1000px" >


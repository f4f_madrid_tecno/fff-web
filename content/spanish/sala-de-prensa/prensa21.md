---
title: "Rueda de prensa de Juventud por el Clima"
summary: "Tras la gran marcha por el clima del 6 de diciembre, en la que más de 500.000 personas salieron a la calle en Madrid, y la inauguración de la Cumbre Social por el Clima, Fridays for Future - Juventud por el Clima organiza una rueda de prensa."
month: "Dic-19"
date: 2019-12-08T12:00:00+01:00
link: "../../../../../images/dic19/191208_Convocatoria_Lunes9.pdf"
---

<embed src="../../../../../images/dic19/191208_Convocatoria_Lunes9.pdf" type="application/pdf" width="100%" height="1000px" >

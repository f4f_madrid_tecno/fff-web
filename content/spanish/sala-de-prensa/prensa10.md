---
title: "Comunicado"
summary: "Este viernes, por primera vez desde que Fridays For Future llegó a Madrid, cambiamos la ubicación de nuestra sentada. Nuestro objetivo siempre ha sido la protección del medioambiente y del planeta en el que vivimos, pero este viernes hemos decidido centrarnos en un ​ problema a nivel local​"
month: "Jun-19"
date: 2019-06-14T17:47:34+01:00
link: "../../../../../images/jun19/20190614_NDP_AireLimpio.pdf"
---

<embed src="../../../../../images/jun19/20190614_NDP_AireLimpio.pdf" type="application/pdf" width="100%" height="1000px" />

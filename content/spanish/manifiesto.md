---
title: "Manifiesto"
date: 2019-11-01T17:47:34+01:00
featured_image: ''
description: "Fridays for future"
---

#### **Fridays for future** es un movimiento juvenil por la defensa de nuestro planeta, una iniciativa estudiantil a escala europea que busca situar la crisis ambiental que atraviesa nuestro planeta en el foco de atención. La comunidad científica lleva años alertándonos de que nos dirigimos a una situación climática incierta, impredecible pero seguro catastrófica.

<img style="height: 130px; width: 130px; float: left; padding: 0.4cm" src="../images/fff_logo.png"> Ya estamos viviendo las primeras consecuencias: cambio climático, extinción masiva de especies, desertificación y destrucción de ecosistemas, acidificación de los océanos, etc. Como jóvenes, como estudiantes, no podemos aceptar el inmovilismo de la clase política ante esta realidad indiscutible que nos va a tocar vivir el día de mañana. Nuestro planeta es nuestro futuro y no podemos quedarnos en casa viendo cómo se destruye. Como dice Greta Thunberg, la activista sueca que empezó con este movimiento, _“¿De qué sirve ir a la escuela si no tenemos futuro?”_. 

Las y los jóvenes europeos nos manifestamos en toda Europa, para incidir en la agenda política, para que se escuche a la comunidad científica, para que se aborde el tema como lo que es, una crisis, y para exigir que se declare el estado de emergencia climática. Somos un grupo de jóvenes diversos y diversas que configuramos un colectivo heterogéneo compuesto por jóvenes estudiantes de colegios, institutos, universidades y grados que nos reunimos cada viernes a título individual para defender nuestro futuro, independientemente de nuestras simpatías políticas, afiliaciones o pertenencia a otros colectivos. 

Estamos seguros y seguras de que estamos a tiempo de poner freno al cambio climático y de revertir sus efectos, aunque no podemos permitirnos tardar mucho más. El Panel Intergubernamental de Expertos sobre el Cambio Climático nos ha alertado de que en tan solo 11 años podemos llegar a un punto de no retorno en materia climática, es por ello que debemos actuar de inmediato, sino es ahora no habrá otro momento. 


Desde Fridays For Future no hacemos reivindicaciones políticas concretas, somos jóvenes, somos estudiantes, no es nuestra competencia. Lo que pedimos es responsabilidad política, queremos que nuestros políticos hablen del cambio climático, que escuchen a la comunidad científica que lleva décadas alertándonos, que busquen y pongan en marcha alternativas sostenibles a la crisis climática y que sean responsables con sus decisiones, que cumplan el acuerdo del clima de París y del IPCC. No exigimos más, solo que se ponga freno a la crisis medioambiental.
<hr>
<img style="float: right; padding: 0.5cm" src="../images/logo-alianza-small.png"> 

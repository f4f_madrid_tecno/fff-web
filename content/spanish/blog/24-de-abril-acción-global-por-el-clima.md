---
layout: blog
title: 24 de abril. Acción global por el clima.
date: 2020-04-15T09:34:13.520Z
banner: /images/photo_2020-04-14_20-37-05.jpg
---
<!--StartFragment-->

<a href="https://fridaysformadrid.org/images/24A-tutorial.png"> <img style="height: auto; width: 100%;" src="../../../../../images/24A-tutorial.png"></a>

Consulta más detalles en [https://2020rebelionporelclima.net/en/accion-global-por-el-clima/](https://2020rebelionporelclima.net/en/accion-global-por-el-clima/).

<hr>

## ¿Por qué es necesario seguir luchando?

Hoy, cuando toda la población sufre en carne propia la pandemia del coronavirus y sus consecuencias, se hace patente lo urgente e ineludible que es aunar esfuerzos para, solidariamente, hacer frente a la enfermedad y sus repercusiones sanitarias y sociales. Unas consecuencias que son sufridas en mayor grado por las personas y los colectivos más vulnerables, que padecen no solo la crisis sanitaria sino la precarización y el empeoramiento de sus ya poco dignas condiciones de vida.

Ese mismo conocimiento que nos hace entender que estamos ante una situación de emergencia, nos lleva a las organizaciones firmantes a continuar con la convocatoria de la *Acción global por el clima* del día 24 de abril. Porque, aunque hoy la crisis que está dando la cara de un modo más crudo es la sanitaria, cada crisis es una crisis y debe ser tratada como tal, la virulencia de una no minimiza la de las otras. La crisis climática sigue siendo una realidad y, pese a estar formalmente declarada como emergencia por el Parlamento Europeo, por el Gobierno español y por otras muchas instituciones, sigue sin ser reconocida como tal por algunos dirigentes, que optan por desoír las indicaciones científicas a través de una inacción culpable.

Este no es momento de interferir en la lucha contra la pandemia ni de detraer recursos de ella y, por eso, esta convocatoria integrada en una coordinación internacional impulsada por el colectivo Fridays for Future bajo el marco de “ Global Strike for Climate”, adopta una forma particular, situando su centro en las redes y llamando exclusivamente a la reflexión y a sentar las bases para actuaciones futuras, más concretas y contundentes.

Porque esta crisis de salud pública ha puesto de manifiesto que esa sensación que teníamos de seguridad absoluta garantizada por la tecnología era absolutamente falsa. También ha mostrado lo dañina que puede ser una crisis si nos pilla desprevenidos, sin planes de prevención y emergencia suficientes que puedan hacerle frente de forma efectiva. Si antes de que sean evidentes sus efectos más dramáticos, no tenemos un plan, no asignamos medios suficientes, no actuamos con convicción y no seguimos las recomendaciones que nos marca la ciencia. Si no actuamos decidida y rápidamente, se alcanzará un cambio climático de tal magnitud y rapidez que haría imposible nuestra adaptación. Sería devastador para la mayoría de los ecosistemas y las sociedades humanas.

La humanidad enfrenta una emergencia climática sin precedentes en la que también es necesario actuar con la responsabilidad de proteger la vida en primer lugar. Esta defensa de la supervivencia debe llevarnos a tomar medidas ambiciosas y drásticas, en otras palabras, a asumir el estado de emergencia climática, impulsando las actuaciones necesarias y no las que se presentan como “políticamente posibles”, antes de que la situación nos desborde. Tratemos la emergencia climática como la crisis que es. Actuar con contundencia hoy frente a la emergencia climática conseguirá que los impactos ambientales y sociales sean mucho menores, aunque gran parte de ellos, ya no seremos capaces de paliarlos.

Estamos al borde de un punto de no retorno marcado por una enorme pérdida de biodiversidad y por el incremento de la temperatura global, que pone en jaque a todo el planeta, incluida la humanidad y sus sociedades. Las consecuencias del cambio climático son, entre otras, un incremento de los grandes incendios forestales, la proliferación de enfermedades de regiones más cálidas, sequías más severas, subida del nivel del mar, también lluvias torrenciales que provocarán inundaciones, movimientos migratorios motivados por la crisis climática…

Y la desigualdad:. En palabras del relator especial de Pobreza Extrema y Derechos Humanos de Naciones Unidas, “el mundo está en riesgo de caer en el apartheid climático, donde los ricos pagan por escapar del sobrecalentamiento, el hambre y las guerras, mientras que el resto del mundo es dejado de lado sufriendo”. Y el resto del mundo somos mayoría.

El calentamiento global es consecuencia directa del modelo de producción y consumo que continuamente se demuestra incapaz de satisfacer las necesidades vitales de las personas, precarizándolas y poniendo en situación de vulnerabilidad a gran parte de la población mundial; de los ecosistemas y el resto de seres vivos que habitan este planeta. Mientras, arriesga nuestra supervivencia como especie, al basarse en la explotación ilimitada de los recursos naturales, impactando de manera injusta en las poblaciones más pobres y vulnerables. Pero en esta crisis, la ciencia también ha señalado la vía de actuación, que no es otra que la descarbonización de nuestro sistema económico, especialmente en las sociedades industrializadas occidentales que tenemos más responsabilidades y mayores capacidades. Un sistema económico que se ha paralizado ante la crisis del coronavirus y que debemos replantearnos antes de reiniciar.

Ante ello, las organizaciones firmantes, de acuerdo con la ciencia, manifestamos que sí, que participamos de la idea de la necesidad de apoyar la salida de las crisis generadas por el coronavirus, pero matizamos que esta no puede reproducir el modelo que nos ha conducido hasta la emergencia ecológica y social actual.Que se puede y debe incorporar lo que hemos aprendido. Que debe basarse en las personas, en sus posibilidades y en sus necesidades, así como en la protección de nuestros recursos naturales. Una idea que resumimos en la frase “salgamos del parón y transformemos la actividad de un modo climática y socialmente justo”.

Sabemos, lo dice la ciencia, que la gravedad de la emergencia nos obliga a adoptar medidas muy profundas, que lo que hagamos en esta década va a condicionar completamente el grado de calentamiento que vamos a sufrir a corto, medio y largo plazo y que, por eso, es ineludible reducir rápidamente ciertos consumos (como el energético), cambiar las pautas de transporte, acelerar la transición energética desde los combustibles fósiles a un modelo 100% renovable, eficiente, sin emisiones contaminantes y justo, especialmente desde la óptica del autoconsumo y la descentralización. Es necesario un cambio de escala, de lo global a local, que ponga en el centro la reducción de las largas cadenas de transporte, la puesta en valor de modelos alimentarios en consonancia con los límites del planeta. Unos esfuerzos que deben conducirnos a una reducción drástica de nuestras emisiones, en línea con las indicaciones científicas y alcanzando la neutralidad lo antes posible.

Solo así será posible hacer frente a las consecuencias del calentamiento global, avanzar hacia una sociedad justa y solidaria y, simultáneamente, reducir el riesgo de otras crisis que el cambio climático alimenta.


**ACTUACIONES PARA RELANZAR LA ACTIVIDAD DE UNA FORMA JUSTA Y SOSTENIBLE**

El indispensable relanzamiento de la actividad no podrá ser considerado social y climáticamente justo si no incorpora dos principios básicos:

* Desde el punto de vista climático, enfrentar una reducción drástica de las emisiones netas de gases de efecto invernadero, en línea con las indicaciones científicas y alcanzando la neutralidad lo más rápidamente posible.
* Desde el punto de vista social, consideración absolutamente prioritaria de las personas y de los colectivos vulnerables, garantizando para ellos unas condiciones de vida dignas.

La aplicación de estos principios se refleja en muchos aspectos vitales, entre los que destacamos los siguientes:

* Reorganización de los sistemas de producción y consumo, definiendo cuáles son las necesidades esenciales de nuestras sociedades para darles prioridad.
* Replanteamiento de las políticas comerciales, nacionales e internacionales, plasmadas o no en Tratados de Comercio e Inversión (TCI), primando los consumos de proximidad y, limitando la dependencia exterior, sea en energía o en cualquier necesidad esencial.
* Priorización de las medidas encaminadas a reducir la pérdida de biodiversidad y a la restauración de los ecosistemas.
* Potenciación de la cohesión social y de la solidaridad, así como del sector público.
* Potenciación del empleo en sectores sostenibles y del reparto del trabajo, así como introducción de medidas de transición justa para sectores y territorios vulnerables a los cambios, avanzando en medidas como la reducción de la jornada, la conciliación…
* Protección de los más vulnerables, sea cual sea la causa de su vulnerabilidad (clase, raza, género…), avanzando hacia una renta básica, de forma que nadie quede atrás.
* La fiscalidad y el gasto público deberán ser acordes con una redistribución justa y con los principios de protección ambiental y de descarbonización.
* Asunción de la deuda climática que, como Estado, hemos adquirido con los países del Sur global, a través de una contribución justa y con garantías suficientes a los fondos de financiación internacionales, de acuerdo con los principios de justicia climática.
* Aceleración de la transición energética desde los combustibles fósiles hacia un modelo 100% renovable, sin emisiones contaminantes, justo y eficiente, apostando decididamente por el autoconsumo y la energía distribuida y pasando por sistemas tarifarios justos.
* Transformación del modelo de movilidad reduciendo el uso del vehículo privado y fortaleciendo las infraestructuras y los servicios del transporte público, especialmente del ferrocarril convencional tanto para mercancías como para pasajeros.
* Transformación de modelo alimentario hacia otro basado en sistemas agroecológicos y agrosilvopastoriles más locales y libres de contaminantes químicos.
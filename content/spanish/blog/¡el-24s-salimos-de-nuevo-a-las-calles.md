---
layout: blog
title: ¡El 24S salimos de nuevo a las calles!
date: 2021-09-07T10:00:44.872Z
banner: /images/photo_2021-09-06_21-33-49.jpg
---
<!--StartFragment-->

A pesar de todos los esfuerzos de lucha por la justicia climática, se sigue muy lejos de escuchar a la ciencia y proteger el presente y futuro medioambiental. Es por ello que queremos seguir resaltando la importancia de tomar acción inmediata frente a las responsabilidades legales y sociales que atañen a las instituciones correspondientes; una exigencia que deben cumplir todos los Gobiernos.

En pleno auge de crisis sociopolíticas y económicas, seguimos enfrentando desastres naturales en todo el mundo, siendo las comunidades que menos contribuyen a ello las que más lo sufren. Es por ello que **los países más contaminantes como España deben asumir su responsabilidad y deuda medioambiental** e iniciar cuanto antes la transición económica y energética que la ciencia y justicia social exige.

Una vez más, hacemos **un llamamiento global a la próxima movilización del 24 de septiembre de 2021** organizada por activistas de todo el mundo para exigir medidas inmediatas, concretas y ambiciosas en respuesta a la actual crisis climática. En el caso de España, en la línea del litigio climático presentado por Greenpeace, Ecologistas en Acción, Oxfam Intermón, la Coordinadora y Juventud Por El Clima; proceso al que llamamos **Juicio por El Clima.**



**Más información dentro de poco.**

<!--EndFragment-->
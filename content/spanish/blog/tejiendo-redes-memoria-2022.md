---
layout: blog
title: "Tejiendo redes: Memoria 2022"
date: 2022-12-31T12:46:22.513Z
banner: /images/memoria-fff-2019-1-.png
---
¡Hola! 

[](<>)\
Te presentamos la [memoria de este año 2022](https://drive.google.com/file/d/1X5xER_ReKJoIm2g0QQiens5khKsi7Ewt/view?usp=sharing), ¡ya es nuestra cuarta memoria! Llevamos más de 200 viernes manifestándonos por la justicia climática. A lo largo de este periodo, hemos organizado sentadas, concentraciones, manifestaciones, acciones, FFFestivales, encuentros…

Estos documentos son textos que buscan recoger todas esas actividades. Los construimos colectivamente, aportando cada una un pequeño fragmento, dejando un poquito de nostras. Los construimos de la misma forma que construimos movimiento, de la misma forma que construimos Fridays for Future Madrid.

Porque Fridays for Future no existiría si no fuera por todas las personas que han estado luchando por la justicia climática. Gracias a todas las que contribuis para que lo siga haciendo.

[](<>)\
Gracias por llenar las calles de alegría y de esperanza. 

Este 2022 nos hemos encontrado.

**En 2023, volvemos, unidas, para seguir exigiendo justicia climática.**

[](<>)\
Te dejamos aquí enlaces a todas nuestras memorias:

\- [Memoria 2022](https://drive.google.com/file/d/1X5xER_ReKJoIm2g0QQiens5khKsi7Ewt/view?usp=sharing)

\- [Memoria 2021](https://drive.google.com/file/d/1YOjvmgbWNMEnIl_gMbcTcMqxtopxoiKi/view?usp=sharing)

\- [Memoria 2020](https://drive.google.com/file/d/1Ib4CJHHTcybzaaPTDVWHbcoh4ahpM2A2/view?usp=sharing)

\- [Memoria 2019](https://drive.google.com/file/d/1OF2hzTfLVDMJNNex8MuE7vdghqQvpkmk/view?usp=sharing)
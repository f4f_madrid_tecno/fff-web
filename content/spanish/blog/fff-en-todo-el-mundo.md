---
title: "FFF en todo el mundo se solidariza con la catástrofe brasileña"
date: 2019-08-22T17:47:34+01:00
tags: ["amazonas", "catástrofe"]
categories: ["Acciones", "Comunicación"]
banner: "images/mani2.jpeg"
---


**Comunicado de prensa internacional de Fridays For Future y Juventud x Clima**

En este mismo momento, los incendios forestales, que se expanden más rápidamente que nunca, están destruyendo la selva amazónica a un ritmo alarmante.

Nuestra casa está literalmente en llamas y los pulmones de nuestro planeta se están convirtiendo en cenizas.

El mes pasado, cuando el IPCC publicaba un nuevo informe especial que reitera la urgencia de actuar para combatir el colapso ambiental que se avecina, se produjeron incendios masivos en el Ártico.

Estos son solo los últimos ejemplos múltiples de la crisis climática provocada por el cambio climático. Nos estamos precipitando hacia una serie incontrolable de eventos que destruirán nuestros ecosistemas y, por lo tanto, nuestro medio de vida en este planeta.

La selva amazónica es la extensión de selva tropical más grande y con mayor biodiversidad del mundo. El impacto de los incendios forestales amazónicos es doble, dado que la selva tropical representa un sumidero de carbono esencial que absorbe el CO2 de la atmósfera, pero también, al quemarlo, libera grandes cantidades de gases de efecto invernadero, acelerando así el círculo vicioso del calentamiento global. Mientras se produce este desastre climático, los medios de comunicación han optado por permanecer en silencio.

Desde que el presidente Jair Bolsonaro llegó al poder, los incendios forestales han aumentado continuamente, alimentados por las sequías, pero también debido a la terrible política ambiental del gobierno brasileño, que ve al Amazonas solo como una bolsa de dinero. Mientras tanto, la economía mundial todavía consume productos brasileños o firma acuerdos comerciales, como la UE con los estados de Mercosur que causarán daños más graves a la Amazonía y a toda la agricultura sudamericana.

Huelga en las embajadas brasileñas

El 23 de agosto, la gente de Fridays For Future, todas llevando el mismo símbolo, irán a la huelga frente a las embajadas de Brasil en todo el mundo. Aunque venimos de muchos países y culturas diferentes, y tenemos diferentes visiones del mundo, todas nos mantendremos unidas detrás de un objetivo común: expresar nuestra consternación y enfado por las políticas del gobierno brasileño que aceleran enormemente la crisis climática, y exigen que se alineen con los objetivos que Fridays for Future espera de todos los gobiernos:

1.  Mantenga el aumento de la temperatura global por debajo de 1.5 ° C en comparación con los niveles preindustriales.

2. Garantizar la justicia climática y la equidad.

3. Escuchar lo que dicen los científicos sobre el tema Instamos a los medios a cumplir con su responsabilidad y a dar a esta tragedia la cobertura que requiere y quien está detrás de ella.

Es nuestra obligación solidarizarnos con el pueblo de Brasil, especialmente las comunidades de primera línea y los pueblos indígenas, que son los más afectados por la destrucción de los bosques amazónicos. Este no es un problema local solo, sino un problema que tiene repercusiones globales y, por lo tanto, instamos a los políticos de todo el mundo a tomar medidas y denunciar la destrucción de este ecosistema esencial.

 

### Daniela Borges – Fridays For Future, Brasil:

“Queremos un gobierno que reconozca la importancia de la naturaleza y que se comprometa a proteger nuestras mayores riquezas naturales. Un gobierno que mantiene un estricto escrutinio contra las compañías que hacen mal uso de nuestros recursos naturales y que antepone los derechos de los pueblos indígenas.”

### David Wicker – Fridays for Future, Italia:

“Las ganancias a corto plazo nunca deben prevalecer sobre las futuras pérdidas devastadoras de las generaciones presentes y futuras. Nuestros pulmones están en llamas. ¿Por qué no estamos haciendo nada para evitar que se conviertan en cenizas?”

### Alejandro Martínez – Fridays For Future, España:

«El Amazonas es uno de los pocos pulmones que sigue teniendo nuestro planeta, el cual nos ayuda a seguir luchando contra la emergencia climática. Lo que estamos viendo no es un proceso natural, es el resultado de la acción del hombre, de las quemas provocadas para deforestar un terreno para así poder explotarlo económicamente. El gobierno de Brasil no ha actuado, ni tiene intención de hacerlo, antes está emergencia, que si no se combate dejará una cicatriz imborrable en uno de los lugares con mayor biodiversidad de nuestro planeta.»

 

En redes sociales:

**#PrayForAmazonia  |  #PrayForTheAmazon  |  #SOSAmazonia  |  #SaveOurLungs**

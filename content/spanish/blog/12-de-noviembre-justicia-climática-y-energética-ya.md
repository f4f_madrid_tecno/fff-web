---
layout: blog
title: "12 de Noviembre: Justicia Climática y energética ya"
date: 2022-11-05T14:54:35.634Z
banner: /images/cartel-madrid-150.jpg
---
Apoyamos la convocatoria de Alianza por el Clima para este 12 de noviembre y salimos a las calles para exigir justicia climática y energética ya. 



# **Manifiesto 12N**

Estamos viviendo una profunda crisis donde a la situación de emergencia climática se suma ahora una continua escalada de precios e incertidumbres por la dependencia de los combustibles fósiles. Además, a la injusticia climática que supone que los menos responsables del problema sean los que sufren las consecuencias de manera más acusada, se suma ahora la injusticia energética, donde los oligopolios están teniendo beneficios extraordinarios mientras la ciudadanía nos empobrecemos. Hacemos un llamamiento a los gobiernos para acelerar la transición energética hacia un modelo eficiente, justo, democrático y renovable; que proteja a las personas más vulnerables y garantice el acceso a un medio ambiente limpio y sano, declarado recientemente como un derecho humano universal por la Asamblea General de Naciones Unidas.

“El retraso en la acción climática significa muertes''. Así valoraba Antonio Guterres, Secretario General de las Naciones Unidas, las conclusiones del informe del Panel Intergubernamental de Cambio Climático (IPCC) sobre los impactos de la crisis climática y la adaptación necesaria. La emergencia climática es una crisis de derechos humanos de una magnitud sin precedentes y la falta de una respuesta adecuada a la crisis climática está provocando violaciones de los derechos humanos. Al menos 3.600 millones de personas viven ya en situación de enorme riesgo climático y la reducción de emisiones, la adaptación y la justicia climática son la única respuesta posible. En España, este mismo año, hemos sentido gravemente los impactos del cambio climático, con terribles olas de calor, incendios forestales, sequía y otros fenómenos extremos. Una realidad ante la que la única respuesta posible pasa por reducir nuestras emisiones, adaptarnos a los cambios y responder con justicia climática.

El modelo de crecimiento ilimitado mediante la continua quema de combustibles fósiles y la destrucción de los ecosistemas debe cesar. La ciencia indica que es necesario realizar reducciones más profundas y más rápidas de gases de efecto invernadero. De hecho, en el Estado español estas reducciones deberían descender hasta alcanzar la neutralidad climática antes de 2040. Sin embargo, los últimos datos de emisiones de la Unión Europea muestran que se está recuperando la tendencia al alza de las emisiones que se registraba antes de la COVID (en España aumentaron un 5,1% en 2021 con respecto al año anterior). Un año y medio después de la entrada en vigor de la Ley de Cambio Climático hay muchas medidas políticas que siguen sin  alinearse con las recomendaciones establecidas por el IPCC. 

Una acción que debe de ampliarse más allá de nuestras fronteras, dando respuesta a la responsabilidad histórica y a la deuda de carbono que nuestro territorio tiene con gran parte del Sur Global. En una situación de clara injusticia, una serie de países enfrenta ya hoy las más graves consecuencias del cambio climático cuando no son responsables de ello. Mientras, los países del Norte Global, incluida España y el resto de la Unión Europea, no aportan la financiación que deben e incluso frenan el desarrollo de instrumentos para acelerar la mitigación y la adaptación al cambio climático y para afrontar globalmente las pérdidas y los daños. 

La  guerra de Ucrania ha dejado patente la estrecha dependencia entre los combustibles fósiles, los conflictos geopolíticos, militares y un sistema económico depredador de la vida. Las consecuencias de la misma se están multiplicando, incrementando al alza los precios de servicios tan básicos como la alimentación o el acceso a la energía, derechos fundamentales para el bienestar humano. Enfrentar la emergencia climática requiere avanzar hacia la soberanía alimentaria y energética, unos cambios que producirán impactos positivos en el planeta al tiempo que reducen nuestra nociva dependencia de las energías fósiles. La promoción de modelos intensivos en la alimentación, del vehículo privado y de las nuevas infraestructuras fósiles son apuestas erróneas con enormes implicaciones negativas sociales y ambientales, tal como recuerda el IPCC. Apostar por reactivar gasoductos o regasificadoras prolonga la dependencia de nuestro país de los combustibles fósiles y frena recursos que necesitamos urgentemente para la transición energética justa y la protección de los derechos humanos de las generaciones presentes y futuras.

Las advertencias climáticas y científicas son cada vez mayores, y las consecuencias cada vez más catastróficas, por ello nos movilizamos. Las organizaciones firmantes exigimos a los gobiernos reunidos en la COP27 que incrementen de forma inmediata la acción climática. Al Gobierno español le exigimos una contribución decidida y comprometida a la financiación de la adaptación de las personas y comunidades más vulnerables del planeta, coherente con su responsabilidad climática, así como fondos adicionales para compensar las pérdidas y daños resultado de  la emergencia climática. Además, el Gobierno debe elevar la ambición de los objetivos de reducción de emisiones de la Ley de Cambio Climático y Transición Energética en línea con las indicaciones científicas y la justicia social. Un proceso de revisión que debe realizarse con la participación de la ciudadanía, las entidades científicas y la sociedad civil organizada, en el que se debe anteponer la protección de un futuro vivible y una transición justa frente a los beneficios de un modelo de crecimiento inviable que nos está llevando a un colapso ambiental y social. 

Ante la organización de la COP 27 en Egipto, la comunidad internacional y, en particular, los países que van a participar en ella incluida España, debe instar a las autoridades egipcias a tomar medidas concretas para revertir la profunda crisis de derechos humanos del país, garantizando la participación de la sociedad civil en el espacio público durante esta cumbre y de forma duradera.

En España, Egipto y en todo el mundo la ciudadanía exige más acción por la justicia climática. El tiempo para actuar se agota y nos jugamos TODO en esta lucha, ANTE LA INACCIÓN DE LOS GOBIERNOS, EL RUGIR DE LA CALLE EXIGIENDO, 

¡QUE NO CAMBIE EL CLIMA, QUE CAMBIE EL SISTEMA!



#### Propuestas de acción

Hay un conjunto de acciones que es urgente acelerar para proteger los derechos humanos y el medio ambiente que habitamos. Es fundamental conseguir un sistema energético que malgaste menos energía, use sólo energías renovables, y en el que la ciudadanía tenga una mayor participación y capacidad de decisión; un sistema de movilidad más sostenible, que implique menos viajes, más cortos, y de forma más saludable para las personas y para el planeta; un sistema alimentario más sostenible, con producción ecológica, más cercana, con menos emisiones de efecto invernadero, con impactos positivos en la biodiversidad, con un uso sostenible del agua y con mejores condiciones laborales. Para esto, necesitamos impulsar verdaderas soluciones como, entre otras: 



* incrementar la acción climática elevando los objetivos incluidos en la ley de cambio climático y transición energética, así como los compromisos internacionales adquiridos, en línea con las indicaciones científicas y la justicia social (objetivos de reducción de emisiones y de apoyo financiero para adaptación y para pérdidas y daños del cambio climático).
* hacer frente a la deuda de carbono adquirida por el Estado Español con el sur global aumentando decididamente la financiación de la adaptación de las personas y comunidades más vulnerables del planeta, y de las pérdidas y daños que ya están sufriendo, asegurando que se cubre la cantidad necesaria.
* acelerar la transición a un nuevo modelo energético mucho más participado por la sociedad basado en el ahorro y la eficiencia energética y en las energías renovables, abandonando las energías fósiles y nucleares, con una adecuada planificación que permita compatibilizar el despliegue renovable con la conservación de la biodiversidad, el reparto de sus beneficios a sus habitantes y territorio y la preservación e impulso de los modos de vida rurales sostenibles.
* democratizar la energía fomentando el autoconsumo y las comunidades energéticas.
* impulsar un modelo alimentario de proximidad, sostenible y ecológico.
* descarbonizar el transporte con un cambio hacia modos más sostenibles (fomentar la bicicleta con redes de calles y carriles exclusivos para las bicis), el transporte público y la disminución de las necesidades de movilidad (por ejemplo, garantizando servicios públicos de proximidad para evitar desplazamientos).
* eliminar la subvención de 20 céntimos/litro al combustible, porque fomenta el consumismo de un recurso que se está agotando y que además provoca gran parte del calentamiento climático.
* aplicar y comprobar que se cumple el límite de velocidad máxima intraurbana de 30 km/h en todas las ciudades; así como la interurbana, limitando más la velocidad interurbana máxima.
* eliminación de vuelos cortos que tengan alternativa ferroviaria.
* mejorar la red y los servicios ferroviarios para vertebrar el territorio y enfriar el planeta.
* asegurar una transición justa para las personas trabajadoras y para los colectivos vulnerables.
* establecer medidas garantistas frente a la pobreza energética y contra la estafa del oligopolio energético.
* recuperar un mundo rural que lleva años enfrentándose al abandono.
* abrir la definición de las políticas climáticas a la participación de la ciudadanía, de las entidades científicas y de la sociedad civil organizada.
* promover un modelo que revalorice y redistribuya los trabajos de cuidados.

POR TODO ELLO NOS VEMOS EL 12 DE NOVIEMBRE EN LAS CALLES
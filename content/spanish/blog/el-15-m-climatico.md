---
title: "El 15-M climático: la nueva revolución juvenil de Europa se extiende a España"
date: 2019-03-12T17:47:34+01:00
tags: ["27S", "Huelga climática"]
categories: ["Comunicación"]
banner: "images/mani4.jpg"
---
Todo comenzó hace apenas unas semanas, con un vídeo de **Greta Thunberg, la activista sueca de 15 años que se ha hecho famosa en toda Europa por ir a protestar cada semana al Parlamento** de su país contra el calentamiento global. Lucas y unos pocos compañeros de clase de la facultad de Ciencias de la Universidad de Girona lo vieron y pensaron que tenían que seguir sus pasos. Al día siguiente, ya estaban protestando frente a la sede de la Generalitat y decididos a continuar con la batalla.

Con este primer gesto, los estudiantes universitarios se unieron a Fridays for future –viernes por el futuro–, **una campaña que sigue la inercia de Thunberg** y suma a jóvenes de toda Europa que se manifiestan todas las semanas para exigir a sus Gobiernos **medidas efectivas contra el cambio climático**.
_“Nos sentimos indefensos y hartos porque año tras año tras año las cumbres por el clima nos están fallando y los Gobiernos incumplen sistemáticamente los acuerdos como el de París”_, explica Lucas a Cuartopoder.es.

En solo unas semanas jóvenes de todo el Estado han conformado Juventud por el Clima, la traducción al castellano del movimiento Fridays for future y se preparan para sumarse a la huelga internacional por el clima que tendrá lugar este viernes, 15 de marzo. Todos los países de Europa cuentan con convocatorias este día y en España ya hay protestas en al menos 23 localidades de comunidadesautónomas como Madrid, Catalunya, Valencia, Andalucía, Extremadura o Galicia.

<img style="display:block; margin-left: auto; margin-right: auto; padding-top: 1cm; padding-bottom: 1cm" src="../../../../../images/mani4.jpg"> 

_“Este es el 15-M climático”_, indica Lucas. “Lo de la fecha es una casualidad afortunada”, explica Irene, otra joven de 19 años estudiante en la Carlos III, organizadora de la convocatoria madrileña. Pero más allá de casualidades, este movimiento recuerda a al del 2011 en varios aspectos: es también una iniciativa “apartidista” impulsada por jóvenes, pero que apela a toda la sociedad porque el motivo afecta a su conjunto. Por otro lado, el movimiento ha surgido a través de las redes sociales“como reacción a lo que ocurría en Europa”, aunque tampoco han faltado las asambleas para organizar la protesta del próximo viernes.

En Girona, otras facultades comenzaron a sumarse a la iniciativa de Lucas y sus compañeros, y ahora toda la Universidad está implicada. En Madrid, estudiantes universitarios llevan reuniéndose desde mediados de febrero en asambleas semanales. A este movimiento verde se han unido **asociaciones estudiantiles como Extinction Rebelion, Abrir Brecha y les apoyan otras organizaciones como Greenpeace**, aunque todas respaldan de manera anónima, sin logos ni banderas. El Sindicato de Estudiantes, que tiene otras demandas políticas ligadas a la ecología, organiza su propia convocatoria este viernes 15 de marzo.
<hr>

##### Publicado por María F. Sánchez en Cuarto Poder : [_El 15-M climático: la nueva revolución juvenil de Europa se extiende a España._](https://www.cuartopoder.es/derechos-sociales/2019/03/12/el-15-m-climatico-la-nueva-revolucion-juvenil-europa-greta-thunberg-espana/)

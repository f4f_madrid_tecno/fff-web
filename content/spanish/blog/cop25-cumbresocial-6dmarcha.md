---
title: "COP25, Cumbre social y 6D marcha por el clima"
date: 2019-12-06T17:47:34+01:00
tags: ["6D", "Huelga climática"]
categories: ["Comunicación", "Acciones", "Logística"]
banner: "images/cop25madrid.png"
draft: False
---

## COP25 y Cumbre Social

<img style="height: 130px; width: 130px; float: left; padding: 0.4cm" src="../../../../../images/6D.png"> Todo sobre lo que vamos a hablar ahora nace a raíz de la Conferencia de la Naciones Unidas sobre el Cambio Climático, o como lo habrás estado escuchando hasta ahora <b>COP25</b> que este año tiene lugar en Madrid del 2 al 13 de Diciembre. Hay varias cumbres sucediendo a la vez, desde FFF Madrid te explicamos las diferencias y te contamos que está sucediendo en este ajetreado mes. 


### LA CUMBRE EN EL ESPACIO INSTITUCIONAL 

Lo que es en sí la propia cubre institucional por el clima, la COP, el espacio donde se reúnen primero los científicos de todo el mundo durante la primera semana y los políticos durante la segunda. Esta cumbre está dividida en dos espacios: 

<p style="color:#0081cf";> ZONA AZUL</p> Donde se llevan a cabo las negociaciones de la COP y la cual, por cierto, tiene un acceso muy restringido. 


<p style="color:#00c9a7";> ZONA VERDE</p> Donde podrás encontrar a las organizaciones sociales y empresas. El objetivo de esta zona es la concienciación ambiental y la creación de un espacio participativo para toda la sociedad civil. Puedes pedir una autorización y acceder a esta zona a través de la página del Ministerio de para la Transición Ecológica o pinchando este 
<a href="https://www.ifema.es/cop25/zona-verde">link</a>.

Fridays For Future Madrid tiene presencia en ambas zonas. 

<hr>

#### EL ESPACIO DE LA CONTRA-CUMBRE 

También llamada <i>Cumbre Social por el Clima</i>, este es un espacio alternativo a la Cumbre Institucional y una confluencia ecologista a nivel internacional. El anuncio, el pasado 1 de noviembre, del traslado de la COP25 de Santiago de Chile a Madrid ha motivado una sinergia sin precedente entre las plataformas convocantes y las cientos de organizaciones sociales y ecologistas españolas e internacionales que se han volcado en montar a contrarreloj la Cumbre Social por el Clima. 


Se realiza entre el 1 y 13 de Diciembre. Durante estos días se celebrarán más de 350 actividades y programas muy interesantes que tendrán lugar en dos espacios: 
<ul>
<li><b>Edificio Multiusos de la Universidad Complutense de Madrid:</b> del 7 al 12 de diciembre, en el Campus de la Moncloa, C/Profesor Aranguren, s/n. 28040 Madrid.</li> 
<li><b>Espacio de Convergencia (reuniones y encuentros</b> - Centro de prensa): del 1-12 de diciembre. Sede histórica de la UGT, C/Hortaleza, 88. 28004 Madrid.</li>
</ul>

Siendo conscientes del claro eurocentrismo que implica la celebración de una COP en un país europeo por tercer año consecutivo, aceptamos el reto de articular protestas y críticas contra estas políticas como una enorme responsabilidad. Lo hacemos desde la rabia e impotencia ante las injusticias y atrocidades que se están cometiendo contra el pueblo chileno, desde la solidaridad y apoyo frente a la decisión de continuar con la celebración de la Cumbre de los Pueblos y de la Cumbre Social por la Acción Climática en Chile, y desde la determinación de intentar generar un espacio donde su voz también pueda ser escuchada. 
Si quieres saber más sobre la contra-cumbre pincha el siguiente <a href="https://cumbresocialclima.net/llamamiento/">link</a>.

<Hr>
Hacemos un llamamiento a participar en la respuesta social a la #COP25 y a tejer red y construir comunidad frente a una crisis climática que es solo el síntoma más visible de un sistema profundamente injusto.

Invitamos a todas las personas y colectivos a participar en la construcción de la #CumbreSocialClima, para rebelarse, proponer y tejer comunidad. Frente a la creciente represión y las estrategias para dividir y desmovilizar a los movimientos, mostraremos más unidad que nunca en la lucha común por la justicia social y ambiental. 

#LosPueblosPorElClima #CumbreSocialClima 

<a href="https://cumbresocialclima.net" target="_blank">https://cumbresocialclima.net </a>
<hr>

<img style="display: block; margin-left: auto; margin-right: auto; max-width:100%" src="../../../../../images/6D_actions.png">
<hr>

## ESPACIO DE DESOBEDIENCIA Y PROTESTA - MARCHA POR EL CLIMA 6D

Desde FFF Madrid nuestra prioridad es la GRAN GRAN, MEGA, SUPER Manifestación de este Viernes 6 de Diciembre, repetimos <b>6 de DICIEMBRE a las 18h en Atocha. </b>

Además de en sí la manifestación, que va a ser histórica y va a ocupar desde Atocha hasta Nuevos Ministerios, va ha haber un escenario donde tocarán Amaral y Makako y Greta Thunberg dará un discurso. 


<img style="display: block; margin-left: auto; margin-right: auto; max-width:100%" src="../../../../../images/6D.jpg">


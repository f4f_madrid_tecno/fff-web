---
layout: blog
title: 3 de marzo por un futuro ecofeminista
date: 2023-02-23T22:16:35.905Z
banner: /images/3m-a4.png
---
El cambio climático no es una crisis que sólo afecta al planeta, sino también a las personas. Pese a hacerse notar en todos los rincones del planeta, no todas las personas lo padecen por igual. Perjudica especialmente a las mujeres y a otras minorías debido a las estructuras del sistema en el que vivimos.

La violencia sistémica lleva a que las desigualdades sociales predominen y empeoren, lo que significa que la crisis climática inevitablemente impactará en mayor medida a grupos sociales que son discriminados y marginalizados por la sociedad.

Necesitamos soluciones interseccionales, donde se considere tanto la emergencia climática como se reconozca a todas aquellas personas que son más afectadas simplemente debido a su identidad.

\*El **viernes 3 de marzo a las 18:00** protestastaremos frente al **Congreso de los Diputados**.  Demandamos que se tomen acciones que nos permitan reparar el daño causado y construir ciudades en las que merezca la pena vivir, ciudades que luchen contra las desigualdades. **Necesitamos un futuro ecofeminista y justicia climática ahora, porque mañana será demasiado tarde.**

¿Te vienes a luchar por la justicia climática y social? 

\#FuturoEcofeminista

\#TomorrowIsToLate

¿Quieres saber más? [Aquí](https://drive.google.com/file/d/1Ho8rzoY02bzR6NYTxtbkzl7FceSkuIe8/view?usp=sharing) dejamos nuestro **manifiesto**.
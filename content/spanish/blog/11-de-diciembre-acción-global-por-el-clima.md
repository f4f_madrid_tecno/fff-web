---
layout: blog
title: "11 De Diciembre Acción Global Por El Clima"
date: 2020-12-09T15:24:25+01:00
banner: /carousel/VIERNES_11.png
---
<!--StartFragment-->

<img style="height: auto; width: 100%;" src="/carousel/VIERNES_11.png">

## Promesa #Fightfor1Point5

Hace cinco años, el 12 de diciembre de 2015, los líderes mundiales acordaron el Acuerdo de París. Al hacerlo, hicieron una promesa al mundo, a quienes están en la línea del frente y a todas las generaciones futuras: abordar la emergencia climática y limitar el aumento de la temperatura global a muy por debajo de los 2 grados.

Desde entonces, los líderes de todo el mundo aún no han tratado la crisis climática como una crisis. Después de cinco años, entendemos que claramente no tiene sentido simplemente pedir a nuestros líderes que cumplan la promesa que hicieron en París. **Es hora de que hagamos nuestras propias promesas.**

Actualmente estamos experimentando cómo se ve un mundo 1,2 ° C más cálido, y esto ya es una catástrofe. Se está quemando, se está inundando, se está derritiendo, las especies se están extinguiendo, la gente se está ahogando y muriendo de sed. Estamos cada vez más cerca de puntos de inflexión desastrosos e irreversibles. **En ausencia de liderazgo, estamos haciendo esta promesa, entre nosotros y con el planeta, NOSOTROS exigimos cambio y responsabilidad. Lucharemos por el mundo que queremos.**

La ciencia es clara sobre las consecuencias de que el calentamiento global alcance los 2 ° C. Sin embargo, los líderes todavía lo discuten como un objetivo. Esto es inaceptable. La realidad actual de 1,2 grados centígrados ya es inhumana y atroz. Los líderes mundiales no tienen derecho a hablar sobre los objetivos de cero neto para 2050 como si esto fuera el colmo de la ambición. Limitar nuestra ambición a cero neto para 2050 es una sentencia de muerte para muchos. Es una bofetada en la cara, cuando la pandemia golpeó, la prioridad de nuestros líderes fue rescatar a la industria de los combustibles fósiles, no a los que estaban en la primera línea de estas crisis. Estos supuestos líderes no tienen derecho a hablar de "ambición".

La necesidad de un cambio rápido y fundamental del sistema es clara. Hasta que tengamos sistemas para proteger a las personas y el planeta, todos, excepto los más ricos, se enfrentan a una mayor incertidumbre y pobreza. La transición no sólo tiene que ser más rápida, sino también más justa, para la mayoría, y no para unos pocos. No podemos volver a la normalidad. La normalidad ya era una crisis, una crisis de desigualdad, destrucción de la naturaleza y el clima. Aquellos que no se dieron cuenta son demasiado privilegiados para hacerlo. El sistema no está roto, fue construido para ser injusto.

Los responsables de la toma de decisiones tienen que tomar una decisión. Mientras tanto, más y más personas se están dando cuenta de que todos los riesgos e injusticias que el mundo está experimentando tienen las mismas causas fundamentales. Cada vez más estamos aprovechando lo que muchos de nosotros hemos olvidado: un sentido de unidad mientras defendemos un mundo más justo, más limpio, más seguro y más saludable. Nuestros supuestos líderes no están luchando por el bienestar del planeta y su gente. Así que nos comprometemos a salir a la calle, a gritar, a la huelga, a construir campañas y a formar conexiones, a exigir más y a convertirnos en movimientos más fuertes y poderosos hasta que estén listos para luchar con nosotros y no contra nosotros.
Depende de nosotros, las personas, en todo el planeta, de cada generación, organizarnos, movilizarnos y lograr el cambio que queremos ver en el mundo. No nos detendremos porque sabemos que merecemos un presente seguro y justo, y un futuro verde y sostenible para todos. 

**Prometemos seguir luchando por el mundo que necesitamos, luchar por 1.5.**

[FIRMA AQUÍ](https://fridaysforfuture.org/fightfor1point5/promise/#form)


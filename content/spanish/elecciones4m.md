+++
title = "Promesa elecciones 4M"
+++


Dentro de seis días tendrán lugar las elecciones a la presidencia de la Comunidad de Madrid, y son importantes, mucho. Muchas de las personas que formamos parte de Fridays For Future somos menores de edad, y aunque nos preocupan las mismas cosas que a ti, no tenemos manera de expresar esa opinión en las urnas.

Por eso te pedimos que votes, por favor, y que lo hagas en nuestro nombre. Que elijas futuro. Te pedimos que a pesar del desencanto, del cansancio, el martes que viene vayas a votar, por ti y porque nosotras no podemos.  

Tenemos un futuro que afrontar, y aunque no se limita solo a un mandato electoral, tiene que empezar a cambiar ya, tiene que empezar ahora, no podemos esperar más. Queremos trabajar por ese futuro todas juntas, luchar por lo que nos parece justo y defender lo que consideramos nuestro. Pero algunas de nosotras aún no tenemos voz electoral para empezar a construir todo esto el 4 de mayo. Por eso te pedimos tu promesa de que vas a votar por el futuro. Prométenos que irás a votar, por todas nosotras, y que cuando estés delante de la urna pensarás en qué futuro quieres construir antes de meter tu papeleta.

Nosotras luchamos por el futuro todos los días, no solo los viernes. Pero el martes que viene no podremos hacerlo, así que te pedimos que lo hagas en nuestro nombre. 


### ¿Nos lo prometes? ¿Votarás futuro?

### <a href="/elecciones4magradecimiento" style="border-radius: 6px; padding: 10px; text-decoration: none; border: #1CA54B solid 3px">Lo prometo</a>
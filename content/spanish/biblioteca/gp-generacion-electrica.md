---
title: "Estudio técnico de viabilidad de escenarios de generación eléctrica en el medio plazo en España"
date: 2019-02-15T17:47:34+01:00
banner: "images/books/gp-generacion-electrica.png"
author: "Greenpeace"
link: "../../../../../books/2019_GP_Generacion-Electrica.pdf"
---

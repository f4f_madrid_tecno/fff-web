---
title: "Trabajadores e infancia en Fukushima: en primera línea de la catástrofe"
date: 2019-02-15T17:47:34+01:00
banner: "images/books/gp-fukushima.png"
author: "Greenpeace"
link: "../../../../../books/2019_GP_Fukushima.pdf"
---

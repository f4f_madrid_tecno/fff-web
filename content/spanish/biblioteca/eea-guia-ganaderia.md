---
title: "Guía jurídico-administrativa para hacer frente a la ganadería industrial"
date: 2019-02-15T17:47:34+01:00
banner: "images/books/eea-ganaderia.png"
author: "Ecologistas en Acción"
link: "../../../../../books/2019_EeA_guia-juridico-administrativa-ganaderia-industrial.pdf"
---

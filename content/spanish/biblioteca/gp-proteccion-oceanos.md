---
title: "Guía para la protección de los océanos - Cómo proteger el 30% de los océanos para 2030"
date: 2019-02-15T17:47:34+01:00
banner: "images/books/gp-proteccion-oceanos-es.png"
author: "Greenpeace"
link: "../../../../../books/2019_GP_ES_ProteccionOceanos.pdf"
---

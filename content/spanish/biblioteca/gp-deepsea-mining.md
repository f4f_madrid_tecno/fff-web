---
title: "In deep water - The emerging threat of deep sea mining"
date: 2019-02-15T17:47:34+01:00
banner: "images/books/gp-deepsea-mining.png"
author: "Greenpeace"
link: "../../../../../books/2019_GP_Deepsea_Mining.pdf"
---

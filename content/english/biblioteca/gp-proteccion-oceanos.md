---
title: "A blueprint for ocean protection - How we can protect 30% of our oceans by 2030"
date: 2019-02-15T17:47:34+01:00
banner: "images/books/gp-proteccion-oceanos-en.png"
author: "Greenpeace"
link: "../../../../../books/2019_GP_EN_ProteccionOceanos.pdf"
---

---
title: "Propuestas de fiscalidad ambiental: avanzando hacia un mundo más justo y sostenible"
date: 2019-02-15T17:47:34+01:00
banner: "images/books/gp-fiscalidad-ambiental.png"
author: "Greenpeace"
link: "../../../../../books/2019_GP_PropuestasFiscalidadAmbiental.pdf"
---

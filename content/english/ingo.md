---
title: "La Ingobernable"
date: 2019-11-01T17:47:34+01:00
featured_image: ''
description: "La ingobernable"
draft: True
---

<img style="display: block; margin-left: auto; margin-right: auto; max-width:100%" src="../images/header_ingo.png">

### Comunicado

<b>Hoy, 13 de noviembre de 2019, han desalojado la Ingobernable.</b>

Hemos intentado escribir este comunicado muchas veces. Hemos intentado ser previsoras, tener listo nuestro mensaje de apoyo. Hemos pensado, una y otra y otra vez, cómo expresar todo lo que sentiríamos cuando llegase el momento.
El momento ha llegado, han desalojado la Ingobernable y, desde Fridays For future Madrid, no estábamos preparadas. ¿Cómo te preparas para que te quiten tu espacio? ¿Cómo escribes algo intentando prever lo que vas a sentir? Qué palabras se usan cuando te echan de tu espacio de reunión, de aprendizaje… 

<i>"Vive hoy como si no nos fueran a desalojar mañana" </i> nos dijo alguien muy sabio, y eso hicimos. El sábado pasado, dedicamos todo el día a nuestra querida offficina, (sí, con tres F) un espacio que nunca habría existido si no fuera en la Ingobernable. Pintamos una puerta de color amarillo, otra de blanco y un árbol enorme en una de las paredes. Cuidamos de nuestro espacio, porque el espacio siempre ha cuidado de nosotras. 

<hr>
 <video style="display: block; margin-left: auto; margin-right: auto; max-width:100%; padding: 0.5cm" controls>
  <source src="../videos/FFIngo.mp4" type="video/mp4">
<i>Amamos la Ingobernable y a todas las ingobernables que la hacen posible.</i>
</video> 

<hr>
En Fridays siempre hemos dicho que solo funcionamos porque somos una familia, y estamos orgullosas de decir que la Ingobernable es parte de ella. Desde Fridays For Future Madrid lo único que nos sale decir es “gracias”. Gracias a un centro que nos ha refugiado, del calor y de la lluvia.  Pero gracias a un centro que nos ha acogido y nos ha enseñado a ser mejores activistas, a empezar la revolución en casa, como las hermanas mayores que no sabíamos que necesitábamos. 

Han desalojado en el peor momento, con la ultraderecha campando a sus anchas en el Congreso y en medio de la peor crisis ecológica de la historia de la Tierra. 
Han desalojado en el mejor momento, con todas nosotras listas para defender nuestro espacio, listas para defender nuestra existencia. 
Hoy han desalojado la Ingobernable, demostrando que nunca han tenido corazón. Hoy han desalojado la Ingobernable y vamos a demostrar que, ahora más que nunca, sabemos organizar nuestra rabia. 

No solo somos <b>ingobernables</b>. Somos invencibles.
Nos vemos en las calles.

<hr>
<img style="float: right; padding: 0.5cm" src="../carousel/fff_logo_Ingo.png" height="140" width="175"> 

---
title: "Manifest"
date: 2019-11-01T17:47:34+01:00
featured_image: ''
description: "Fridays for future"
---

#### **Fridays for Future** is a youth movement defending our planet. We are a Europe-wide student initiative that seeks to place at the center of attention the environmental crisis that threatens our planet. This is a crisis that the scientific community has been warning us for years about: that we are headed toward an uncertain climate situation, unpredictable but most certainly catastrophic.

<img style="height: 130px; width: 130px; float: left; padding: 0.4cm" src="../images/fff_logo.png"> We are already experiencing the first consequences of this crisis, with climate change, mass extinction, desertification and destruction of ecosystems, ocean acidification, and more. As young people and as students, we cannot accept the inaction of the political class in the face of the indisputable reality that will continue to play out in the days to come. Our planet is our future, and we cannot stay at home watching as it is destroyed. As Greta Thunberg, the Swedish activist who started the Fridays for Future movement, says:  _“What’s the point of going to school if we have no future?”_. 

Young Europeans are demonstrating all across the continent to influence the political agenda and demand that the scientific community be listened to, that this issue be tackled for what it is—a crisis—and that a state of climate emergency be declared. We are young people organized in a diverse collective—including students in primary schools, secondary schools, and universities, as well as graduates—who meet each Friday on an individual basis, independent of our political affiliations or our membership in other groups, to defend our future.

We certainly still have time to put a stop to climate change and reverse its effects, but we cannot allow ourselves to wait any longer. The Intergovernmental Panel on Climate Change (IPCC) has warned us that when it comes to the climate, we could reach the point of no return in just 11 years. That is why we must act immediately: we won’t have another chance. 

At Fridays for Future, we do not make concrete political demands—we are youth and we are students, and it is not our job to determine specific policies. What we are asking for is political accountability: we want our politicians to speak of climate change, to listen to the scientific community that has been warning us for decades, and to search for and implement sustainable solutions to the climate crisis. We want them to be responsible with their decisions, and to comply with the terms of the Paris climate agreement and the recommendations of the IPCC. We do not ask for more—only that they put a stop to the environmental crisis. 
<hr>
<img style="float: right; padding: 0.5cm" src="../images/logo-alianza-small.png"> 

---
title: "Comunicado"
summary: "Este viernes habrá pasado casi una semana desde las elecciones al Parlamento Europeo del pasado día 26. Los ciudadanos de Europa han escogido a sus nuevos líderes que gobernarán durante los próximos cuatro años, un momento clave para la lucha contra la crisis climática."
month: "May-19"
date: 2019-05-31T17:47:34+01:00
link: "../../../../../images/may19/20190531_NDP_EconomiaCircular.pdf"
---

<embed src="../../../../../images/may19/20190531_NDP_EconomiaCircular.pdf" type="application/pdf" width="100%" height="1000px" />

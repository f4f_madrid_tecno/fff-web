---
title: "Comunicado"
summary: "Esta semana, una terrible noticia ha dado la vuelta al mundo: ​ Groenlandia se derrite​ . En esta ocasión, y como suele pasar, la noticia se ha viralizado gracias a la impactante fotografía
de unos perros arrastrando un trineo sobre el ​ hielo derretido​."
month: "Jun-19"
date: 2019-06-21T17:47:34+01:00
link: "../../../../../images/jun19/21_06.pdf"
---

<embed src="../../../../../images/jun19/21_06.pdf" type="application/pdf" width="100%" height="1000px" />

---
title: "Cientos de organizaciones sociales bajo el paraguas de Fridays for Future, 2020 Rebelión por el Clima y Alianza por el Clima llaman a una acción global por el clima el próximo 24 de abril."
summary: "Las organizaciones convocan a la ciudadanía a una manifestación por el clima el próximo viernes 24 de abril a las 22h mediante la proyección de sombras y sonidos desde los balcones."
month: "Abr-20"
date: 2020-04-15T08:47:34+01:00
link: "../../../../../images/abr20/200415_Acción24A.pdf"
---

<embed src="../../../../../images/abr20/200415_Acción24A.pdf" type="application/pdf" width="100%" height="1000px" />
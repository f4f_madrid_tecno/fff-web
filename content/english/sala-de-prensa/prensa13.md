---
title: "Comunicado"
summary: "Este viernes es distinto. Como todos sabemos, ​ Alfredo Pérez Rubalcaba​ , quien fuera ministro, hombre de estado y profesor, ha fallecido hoy, a los 67 años de edad."
month: "May-19"
date: 2019-05-10T17:47:34+01:00
link: "../../../../../images/may19/20190510_NDP_Rubalcaba.pdf"
---

<embed src="../../../../../images/may19/20190510_NDP_Rubalcaba.pdf" type="application/pdf" width="100%" height="1000px" />

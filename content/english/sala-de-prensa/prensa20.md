---
title: "Greta Thunberg will give a press conference in Spain."
summary: "The environmental activist will be addressing all of the media’s questions before taking part in the Strike for Climate on the 6th which will start at Atocha and finish at Nuevos Ministerios."
month: "Dic-19"
date: 2019-12-05T12:00:00+01:00
link: "../../../../../en/images/dic19/191205_NDPEN_Greta.pdf"
---

<embed src="../../../../../en/images/dic19/191205_NDPEN_Greta.pdf" type="application/pdf" width="100%" height="1000px" >

---
title: "Fridays for future Madrid valora la declaración de emergencia climática del gobierno como “necesaria, pero insuficiente”"
summary: "Para el grupo ecologista, fijar los objetivos a 2050 no lleva a las “medidas sin precedentes” que reclaman desde hace ya un año. "
month: "Jan-20"
date: 2020-01-21T12:00:00+01:00
link: "../../../../../images/ene20/20200121_FFFM_Com_NDP_DeclaracionEmergencia.pdf"
---

<embed src="../../../../../images/ene20/20200121_FFFM_Com_NDP_DeclaracionEmergencia.pdf" type="application/pdf" width="100%" height="1000px" >


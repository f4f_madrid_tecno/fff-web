---
title: "Comunicado"
summary: "Un viernes más los estudiantes del mundo pararemos para protestar contra el cambio climático y la inacción de la clase política."
month: "May-19"
date: 2019-05-17T17:47:34+01:00
link: "../../../../../images/may19/20190517_NDP_Estudio.pdf"
---

<embed src="../../../../../images/may19/20190517_NDP_Estudio.pdf" type="application/pdf" width="100%" height="1000px" />

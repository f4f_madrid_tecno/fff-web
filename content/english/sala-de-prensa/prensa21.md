---
title: "Juventud por el Clima press conference"
summary: "After the rally for the climate of the last Friday, December 6th, during which more than half a million people took to the streets of Madrid, and the inauguration of the Social Summit for Climate, Juventud por el Clima - Fridays For Future Spain will offer a press conference"
month: "Dic-19"
date: 2019-12-08T12:00:00+01:00
link: "../../../../../en/images/dic19/191208_ENConvocatoria_Lunes9.pdf"
---

<embed src="../../../../../en/images/dic19/191208_ENConvocatoria_Lunes9.pdf" type="application/pdf" width="100%" height="1000px" >


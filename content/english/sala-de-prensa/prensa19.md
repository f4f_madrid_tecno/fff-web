---
title: "Presentación de la Marcha por el Clima y la Cumbre Social por el Clima"
summary: "En el contexto de la COP25 se producirán dos movilizaciones mundiales en Santiago de Chile y en Madrid, unas manifestaciones que con una voz única trasladarán que frente a la inacción de los gobiernos las personas estamos dispuestas a plantar cara ante la emergencia climática."
month: "Nov-19"
date: 2019-11-04T12:00:00+01:00
link: "../../../../../images/nov19/20191104_CdP_Llamamiento.pdf"
---

<embed src="../../../../../images/nov19/20191104_CdP_Llamamiento.pdf" type="application/pdf" width="100%" height="1000px" >


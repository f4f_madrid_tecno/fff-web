---
title: "Comunicado"
summary: "El domingo pasado los españoles decidieron a sus nuevos representantes en las urnas. Dirigirán el país durante un momento clave en la lucha contra el cambio climático, pero no parece que lo sepan."
month: "May-19"
date: 2019-05-03T17:47:34+01:00
link: "../../../../../images/may19/20190503_NDP_Plastico.pdf"
---

<embed src="../../../../../images/may19/20190503_NDP_Plastico.pdf" type="application/pdf" width="100%" height="1000px" />

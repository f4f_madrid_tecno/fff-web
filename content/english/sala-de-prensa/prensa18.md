---
title: "Juventud por el Clima convoca movilizaciones en todo el Estado Español el próximo viernes 29N por motivo del Global Action Day"
summary: "Juventud por el Clima, el colectivo ecologista juvenil impulsor del movimiento internacional Fridays For Future en España, hace un nuevo llamamiento a sumarse a la convocatoria de las movilizaciones, concentraciones y actividades reivindicativas que se realizarán el próximo viernes 29 de noviembre en más de 20 ciudades del Estado Español."
month: "Nov-19"
date: 2019-11-29T12:00:00+01:00
link: "../../../../../images/nov19/191129_JxC_Com_NDP_29N.pdf"
---

<embed src="../../../../../images/nov19/191129_JxC_Com_NDP_29N.pdf" type="application/pdf" width="100%" height="1000px" >


---
title: "Universidad y escuela"
date: 2019-02-15T17:47:34+01:00
banner: "images/mani1.jpg"
draft: True
---
Montero reconoce que esta iniciativa, en España, está por ahora “más arrancada” en entornos universitarios, pero confía en poder movilizar también a estudiantes más jóvenes, como ha ocurrido en lugares como Bélgica o Reino Unido, países en los que también existe una “mayor tradición de asociacionismo juvenil”.

La joven activista ha subrayado la importancia de la implicación del entorno educativo y familiar, donde los padres y los profesores pueden ayudar a fomentar este tipo de actitudes.

En este sentido, desde Juventud por el Clima tratarán de “involucrar a las asociaciones de padres” y al cuerpo docente de los centros educativos para propiciar que una ola de cambio que surja desde dentro.

Por otra parte el Sindicato de Estudiantes ha anunciado que analizará en su XIX congreso el próximo sábado 23 y domingo 24 las acciones que tiene previsto llevar a cabo contra el cambio climático, con ocasión de la huelga estudiantil del 15 de marzo. EFEverde


---
title: "Más de 300 organizaciones se suman a la Huelga Mundial por el Clima"
date: 2019-09-12T17:47:34+01:00
tags: ["27S", "by2020"]
categories: ["Comunicación"]
banner: "images/mani3.jpeg"
draft: True
---
**La convocatoria de Huelga Mundial por el Clima reúne el apoyo de más de 300 organizaciones que han firmado el manifiesto en defensa del futuro, de un planeta vivo y de un mundo justo.**

- Las organizaciones firmantes piden que se declare de manera inmediata la emergencia climática y se tomen las medidas concretas necesarias para reducir rápidamente a cero las emisiones netas de gases de efecto invernadero
- Exigen un nuevo modelo socioecológico que no comprometa la supervivencia de la vida tal y como la conocemos
- Más de 300 organizaciones ya han mostrado su apoyo a la convocatoria de Huelga Mundial por el Clima del próximo 27 de septiembre
- Numerosas ciudades y pueblos acogerán diversos actos el próximo 27 de septiembre en apoyo a la Huelga Mundial por el Clima. Una fecha que se produce en el marco de las movilizaciones por el clima, a nivel global, y de la semana de acción del 20 al 27 de septiembre convocada por Fridays for Future

El próximo 27 de septiembre se celebra la Huelga Mundial por el Clima con un alcance global y que, en el caso de España, cuenta con el apoyo de más de 300 organizaciones, adheridas al [manifiesto](https://drive.google.com/file/d/1xUd2VtLz5ltlD8f64NFRh724gGrm8DxN/view) publicado el pasado mes de julio en defensa del futuro, de un planeta vivo y de un mundo justo. Una huelga que tiene como objetivo exigir a los gobiernos medidas efectivas ante la emergencia climática. Esta crisis climática es el mayor reto al que se enfrenta la humanidad y, por este motivo y con el fin de dar respuesta a la magnitud del problema, la sociedad se está coordinando a nivel mundial. Durante la semana del 20 al 27 de septiembre tendrán lugar numerosas acciones en multitud de municipios de todo el Estado, que culminarán en movilizaciones masivas en todo el mundo el 27 de septiembre.

Durante las últimas semanas se han llevado a cabo numerosos gestos de apoyo a la Huelga Mundial por el Clima por parte de distintos actores sociales, de la comunidad científica y de la artística, que reflejan la necesidad de abordar una transición ecológica urgente y justa. Se espera que estos gestos de apoyo se redoblen en las próximas semanas y que sirvan de aliciente para llamar a la participación a toda la sociedad en las acciones convocadas. Acciones en forma de manifestaciones, huelgas estudiantiles y de consumo, cierres de lugares de trabajo, movilizaciones y concentraciones en los centros de trabajo y en las calles… Un gran grito común y unitario en la lucha climática.

La crisis climática, consecuencia directa del modelo de producción extractivista, basada principalmente en el uso de combustibles fósiles y del consumo globalizado actual, pone en riesgo la supervivencia humana y la de un gran número de otras especies y ecosistemas, afectando especialmente a las poblaciones más empobrecidas y vulnerables.

Desde hace décadas la comunidad científica alerta del deterioro de un gran número de ecosistemas, tanto terrestres como marinos, así como del punto de no retorno frente al cambio climático. Los recientes informes sobre el estado de la biodiversidad del IPBES (Plataforma Intergubernamental de Biodiversidad y Servicios Ecosistémicos) señalan que cerca de un millón de especies entre animales y plantas se encuentran al borde de la extinción como consecuencia de las actividades humanas. Asimismo, el último informe del Grupo Intergubernamental de Expertos sobre el Cambio Climático (IPCC), publicado el pasado agosto, señala que la reducción de las emisiones de gases de efecto invernadero de todos los sectores es el único modo de mantener el calentamiento global por debajo de los críticos  1,5 °C.

No responder con suficiente rapidez y contundencia a la emergencia climática, ecológica y civilizatoria supondrá la muerte y el aumento de la pobreza extrema para millones de personas, además de la extinción de muchas especies e, incluso, de ecosistemas completos.

Por todo ello, y en defensa del presente y del futuro, de un planeta vivo y de un mundo justo, las personas y colectivos firmantes del [manifiesto](https://drive.google.com/file/d/1xUd2VtLz5ltlD8f64NFRh724gGrm8DxN/view) efectúan un llamamiento a toda la sociedad (ciudadanía y diferentes actores sociales, ambientales, de cooperación, sindicales, etc.) para que el próximo 27 de septiembre se unan a las distintas movilizaciones previstas: huelga estudiantil, huelga de consumo para construir estrategias de consumo alternativo que respeten los derechos, la vida y los límites biofísicos del planeta, y huelga laboral en el sector de la enseñanza andaluz convocada por CGT. Asimismo, organizaciones sindicales convocarán asambleas en los centros de trabajo para pedir medidas de transición ecológica y justa en las empresas; mientras que algunas organizaciones ecologistas y sociales realizarán un cierre de 24 horas de sus oficinas e instan a otros actores a un cierre total o parcial durante el desarrollo de la manifestación.

Las organizaciones y plataformas convocantes hacen además un llamamiento a toda la población para que en las oficinas, colegios, ayuntamientos y otros lugares de trabajo se realicen concentraciones simbólicas de cuatro minutos y 15 segundos, entre las 11 y las 12 de la mañana de ese mismo día. Un acto que sirva de reflexión en torno a la gran señal de alarma que supuso alcanzar el pasado mes de abril una concentración de partículas de dióxido de carbono en la atmósfera de 415 ppm (partes por millón), nivel que no se registraba desde hacía tres millones de años,

Este [manifiesto](https://drive.google.com/file/d/1xUd2VtLz5ltlD8f64NFRh724gGrm8DxN/view), que cuenta ya con el apoyo de más de 300 organizaciones, es solo el comienzo. En las próximas semanas se pedirá el apoyo individual de aquellas personas que quieran sumarse a la exigencia de adoptar colectivamente las medidas necesarias para enfrentar la crisis ecológica y social actual.

A continuación, se presenta una lista de las movilizaciones confirmadas para el próximo 27 de septiembre. Se irán actualizando en los próximos días a través de las páginas web de las plataformas convocantes [Juventud por el Clima](http://juventudxclima.es/), [2020 Rebelión por el Clima](https://2020rebelionporelclima.net/) y [Alianza por el Clima](http://alianza-clima.blogspot.com/).

